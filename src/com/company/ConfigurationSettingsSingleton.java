package com.company;

public class ConfigurationSettingsSingleton {

    //Ensures different threads can work without crashing.
    private static volatile ConfigurationSettingsSingleton instance;
    private String path;

    private ConfigurationSettingsSingleton() {
    }

    public static ConfigurationSettingsSingleton getInstance() {

        if (instance == null) {
        //Only one thread may execute this code at a given time. (Sync)
        synchronized (ConfigurationSettingsSingleton.class) {
            if (instance == null) {
                instance = new ConfigurationSettingsSingleton();
            }
        }
    }
     return instance;
    }

    public static void setInstance(ConfigurationSettingsSingleton instance) {
        ConfigurationSettingsSingleton.instance = instance;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
