package com.company;

public class Main {

    public static void main(String[] args) {


        ConfigurationSettingsSingleton singleton = ConfigurationSettingsSingleton.getInstance();
        singleton.setPath("testfolder");
        FileProcessor print = new FileProcessor();
        print.printContents(singleton.getPath(),"/test.txt");

    }
}
//    Create a console application which performs operations on the contents of text files in a specified folder.
//
//        The path to the folder must be contained in a simple ConfigurationSettingsSingleton object (ConfigurationSettingsSingleton), which is set when the program starts.
//
//        Create a class called FileProcessor, which contains method called printContents(String fileName) which will read and print the contents
//        of the file name specified to the console window. You will need to include the base folder path value from the singleton to accomplish this.
//
//        Look at https://howtodoinjava.com/java8/read-file-line-by-line/ for a simple way to read all the lines of a file to save yourself some time.
//
